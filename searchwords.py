#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

"""
Programa creado por Alberto García Zafra
"""

import sortwords
import sys

"""
Funcíon que buscará si la palabra se encuentra dentro de la lista y,
en caso afirmativo, devolverá el índice de esta palabra dentro de la lista.
En caso negativo levantará la excepción Exception.
"""


def search_word(word, words_list):
    found = False
    for i in words_list:
        if sortwords.equal(word, i):
            index1 = words_list.index(i)
            found = True
            return index1

    if not found:
        raise Exception


"""
Función principal que lee los parámetros escritos por línea de comandos(palabra y lista),
comprueba que se hayan escrito el número correcto. 
Con esos parámetros se llama a distintas funciones, la primera, importada de sortwords, para ordenar la lista y
la segunda dentro del propio programa para obtener el índice de la palabra dentro de la lista y poder imprimirlo.
En caso de no encontrarse la palabra dentro de la lista, captura la excepción lanzada.
"""


def main():
    words_list = sys.argv[2:]
    word = sys.argv[1]

    if len(sys.argv) < 3:
        sys.exit("At least two arguments are needed")

    try:
        ordered_list = sortwords.sort(words_list)
        index = search_word(word, ordered_list)
        sortwords.show(ordered_list)
        print(index)
    except Exception:
        sys.exit("Word not found")


if __name__ == '__main__':
    main()
